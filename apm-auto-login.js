
(function() {
    'use strict';
    console.log('Bismillah');

    const _ = (...x)=>{
        console.log('el',x);
        return window.jQuery(...x)
    }
    function impor(url,_async=false){
        var el = document.createElement('script');
        el.type = 'text/javascript';
        el.async = _async;
        el.src = url;
        document.head.append(el);
    }

    impor('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.js');
    impor('https://raw.githubusercontent.com/uzairfarooq/arrive/master/minified/arrive.min.js');

    _('head')
        .append(_("<script />", {src: 'https://cdn.jsdelivr.net/npm/kioskboard@2.3.0/dist/kioskboard-aio-2.3.0.min.js'}))
        .append('<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/kioskboard@2.3.0/dist/kioskboard-2.3.0.min.css" type="text/css" />')
        .append(_('<style />').html(`
        #KioskBoard-VirtualKeyboard {
          z-index: 99999 !important;
          transform: scale(0.70);
          transform-origin: bottom center;
        };

        `))
    ;

    var username = 'apmrajal'
    var password = '12345'

    var selectorPengguna = '[placeholder="[Masukkan ID Pengguna Anda]"]'
    var selectorPassword = '[placeholder="[Masukkan Kata Sandi Anda]"]'
    var selectorMasuk = 'a.x-btn[data-componentid="button-1040"]'
    var selectorToolbar = '#toolbar-1046-innerCt'
    var selectorToolbarMenu = 'span.fa-navicon'
    var selectorTombolDaftar = 'div.fas.fa-sign-in-alt'
    var selectorApm = '.x-treelist-item-text:contains(Anjungan Pendaftaran Mandiri)'
    var selectorMarginInput = '#component-1125' //.x-component.x-component-default'
    var selectorInputQrcode = '#textfield-1127-inputEl'

    var timeoutForm = 2000
    var timeoutLogin = 2000
    var timeoutMenu = 2000
    var timeoutApm = 3000
    var timeoutTombolDaftar = 1000

    var keyboardAttribute = {
        "data-kioskboard-type":"numpad",
        "data-kioskboard-placement":"bottom",
        "data-kioskboard-specialcharacters":"true"
    }
    var keyboardOpsi = {
        keysArrayOfObjects: [{"0": "A"},],
        keysSpecialCharsArrayOfStrings: null,
        language: 'id',
        theme: 'material',
        autoScroll: true,
        capsLockActive: false,
        allowRealKeyboard: true,
        allowMobileKeyboard: true,
        cssAnimations: true,
        cssAnimationsDuration: 360,
        cssAnimationsStyle: 'slide',
        keysAllowSpacebar: false,
        keysSpacebarText: 'Space',
        keysFontFamily: 'sans-serif',
        keysFontSize: '22px',
        keysFontWeight: 'normal',
        keysIconSize: '25px',
        keysEnterText: 'Enter',
        keysEnterCallback: undefined,
        keysEnterCanClose: false,
        keysNumpadArrayOfNumbers: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    };


    document.arrive(selectorPengguna,login)

    function klikTombolDaftar(){
        _(selectorMarginInput).css('margin','0px 0px 50px 0px')
        _(selectorInputQrcode).attr(keyboardAttribute);
        window.KioskBoard.run(selectorInputQrcode,keyboardOpsi);
        let tombolDaftar = _(selectorTombolDaftar);
        tombolDaftar[0].click();
    }

    function login(el){
        let useridEl = _(el)
        let passwordEl = _(selectorPassword)
        let masukEl = _(selectorMasuk)
        useridEl.val(username)
        passwordEl.val(password)
        setTimeout(()=>{
            let isLoginFormVisible = _('.login-form').is(":visible")
            if (isLoginFormVisible === true){
                console.log('belum login')
                let klikLogin = ()=>masukEl[0].click()
                setTimeout(klikLogin,timeoutLogin)
            }else{
                console.log('sudah login')
                let tombolMenu = _(selectorToolbar).find(selectorToolbarMenu)
                let tombolApm = _(selectorApm)

               setTimeout(()=>tombolMenu[0].click(),timeoutMenu)
               setTimeout(()=>{
                   tombolApm[0].click();
                   setTimeout(klikTombolDaftar,timeoutTombolDaftar)
               },timeoutApm)

            }
        },timeoutForm)
    }
})();